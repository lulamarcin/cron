# Cron Application


## Project build

mvn clan package

## Projet run

java -jar target/cron-0.0.1-SNAPSHOT.jar */15 0,15 * 1-5 /usr/bin/find

## Description of the arguments in order

1. minute
2. hour
3. day of month
4. month
5. day of week
6. command

## Supported values

#### For all arguments

```
* any value
, value list separator
- range of values
/ step values
```

#### 1. For minute:

```
0-59
```

#### 2. For hour:

```
0-23
```

#### 3. For day of month:

```
1-31
```

#### 4. For month:

```
1-12
JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC
```

#### 5. For day of week:

```
0-6
SUN, MON, TUE, WED, THU, FRI, SAT
```

## Testing

You can check results by nice online cron editor: [https://crontab.guru/](https://crontab.guru/)

# Implementation description.

Implementation in accordance with [**SOLID**](https://pl.wikipedia.org/wiki/SOLID) guidelines.

The implementation was carried out divided into
api [controller](src/main/java/com/shift4/cron/api/impl/CronControllerImpl.java)
and [domain](src/main/java/com/shift4/cron/domain/impl/CronServiceImpl.java). We can access the mechanism through the
public [interface](src/main/java/com/shift4/cron/domain/CronService.java). The implementation has been hidden
in packages - so that the developer does not become dependent on
the implementation while developing the code. This will make it easier to develop implementations in the future.

In the domain service, we use
an [abstract factory](src/main/java/com/shift4/cron/domain/generator/impl/CronGeneratorAbstractFactoryImpl.java) -
creating a [generator](src/main/java/com/shift4/cron/domain/generator/CronGenerator.java). This factory manages
specific [factories](src/main/java/com/shift4/cron/domain/generator/impl/CronGeneratorFactory.java)
corresponding to supported expressions. During the build method, the abstract factory looks for factories that support
the expression and builds a number generator based on that. If the expression is not supported or contains invalid
values (out of range, for example) -
a [business exception](src/main/java/com/shift4/cron/domain/exception/CronExpressionException.java) is thrown.

The organization of the code into factories - aims to facilitate the addition of further implementations in the future -
supporting new expressions.

The solution is configurable - in [**application.yml**](src/main/resources/application.yml) - you can configure the
details of each expression.

It's worth getting acquainted with the integration test:
[***CronControllerImplIntegrationTest.java
***](src/test/java/com/shift4/cron/api/impl/CronControllerImplIntegrationTest.java)

It shows the possibilities of this implementation.