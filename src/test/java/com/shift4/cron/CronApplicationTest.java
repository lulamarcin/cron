package com.shift4.cron;

import lombok.extern.java.Log;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@SpringBootTest(webEnvironment = NONE, classes = CronApplication.class, args = {"*/15", "0", "1,15", "*", "1-5", "/usr/bin/find"})
@Log
class CronApplicationTest {

    @Test
    void loadContext() throws Exception {
        //then
        log.info("Context successfully loaded. Example args was parsed");
    }

}