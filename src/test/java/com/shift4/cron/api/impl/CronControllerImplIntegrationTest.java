package com.shift4.cron.api.impl;

import com.shift4.cron.CronApplication;
import com.shift4.cron.api.CronController;
import com.shift4.cron.domain.CronService;
import com.shift4.cron.domain.exception.CronExpressionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = NONE, classes = CronApplication.class)
class CronControllerImplIntegrationTest {

    @Autowired
    private CronService service;

    @Autowired
    private CronController uderTest;

    private static List<Arguments> provideCronExpressionsAndInterpretations() {
        return List.of(
                Arguments.of("Example test should working",
                        List.of("*/15", "0", "1,15", "*", "1-5", "/usr/bin/find"), List.of(
                                "minute 0 15 30 45",
                                "hour 0",
                                "day of month 1 15",
                                "month 1 2 3 4 5 6 7 8 9 10 11 12",
                                "day of week 1 2 3 4 5",
                                "command /usr/bin/find"
                        )),
                Arguments.of("* expression should return all possibles values",
                        List.of("*", "*", "*", "*", "*", "doSomething"), List.of(
                                "minute 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59",
                                "hour 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23",
                                "day of month 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31",
                                "month 1 2 3 4 5 6 7 8 9 10 11 12",
                                "day of week 0 1 2 3 4 5 6",
                                "command doSomething"
                        )),
                Arguments.of("Single number should return single value",
                        List.of("0", "1", "2", "3", "4", "doSomething"), List.of(
                                "minute 0",
                                "hour 1",
                                "day of month 2",
                                "month 3",
                                "day of week 4",
                                "command doSomething"
                        )),
                Arguments.of("Range expression should return values from this range",
                        List.of("0-2", "1-3", "2-4", "5-7", "1-3", "doSomething"), List.of(
                                "minute 0 1 2",
                                "hour 1 2 3",
                                "day of month 2 3 4",
                                "month 5 6 7",
                                "day of week 1 2 3",
                                "command doSomething"
                        )),
                Arguments.of("Coma separated expression should return sum of this interpretation",
                        List.of("0,2", "1,3", "2,4", "5,7", "1,3", "doSomething"), List.of(
                                "minute 0 2",
                                "hour 1 3",
                                "day of month 2 4",
                                "month 5 7",
                                "day of week 1 3",
                                "command doSomething"
                        )),
                Arguments.of("Step expression should return step interpretation",
                        List.of("*/15", "2-23/5", "3/10", "4/7", "0/2", "doSomething"), List.of(
                                "minute 0 15 30 45",
                                "hour 2 7 12 17 22",
                                "day of month 3 13 23",
                                "month 4 11",
                                "day of week 0 2 4 6",
                                "command doSomething"
                        )),
                Arguments.of("For month and day of week w can use alternatives",
                        List.of("0", "0", "1", "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC", "SUN,MON,TUE,WED,THU,FRI,SAT", "doSomething"), List.of(
                                "minute 0",
                                "hour 0",
                                "day of month 1",
                                "month 1 2 3 4 5 6 7 8 9 10 11 12",
                                "day of week 0 1 2 3 4 5 6",
                                "command doSomething"
                        )),
                Arguments.of("For month and day of week we can use alternatives in other functions",
                        List.of("0", "0", "1", "JAN,FEB-3,1-MAR,APR/3,5/MAY,JUN-JUL/AUG,SEP-OCT/2,NOV-12/DEC", "SUN,MON-TUE,WED/THU,1-FRI/3,SAT/2", "doSomething"), List.of(
                                "minute 0",
                                "hour 0",
                                "day of month 1",
                                "month 1 2 3 4 5 6 7 9 10 11",
                                "day of week 0 1 2 3 4 6",
                                "command doSomething"
                        ))
        );
    }

    private static List<Arguments> provideWrongCronExpressionsAndExpectedExceptions() {
        return List.of(
                Arguments.of("If expressions count is less than 6, then throw exception",
                        List.of("1", "1", "1", "1", "1"),
                        IllegalArgumentException.class, "Incompatible number of arguments."
                ),
                Arguments.of("If expressions count is more than 6, then throw exception",
                        List.of("1", "1", "1", "1", "1", "doSomething", "extra arg"),
                        IllegalArgumentException.class, "Incompatible number of arguments."
                ),
                Arguments.of("If expressions count is 0, then throw exception",
                        List.of("1", "1", "1", "1", "1", "doSomething", "extra arg"),
                        IllegalArgumentException.class, "Incompatible number of arguments."
                ),
                Arguments.of("If expression value is out of range, then throw exception",
                        List.of("0", "0", "0", "0", "0", "doSomething"),
                        CronExpressionException.ValueOutOfRange.class, "Value is out of range."
                ),
                Arguments.of("If expression is not supported, then throw exception",
                        List.of("**", "1", "1", "1", "1", "doSomething"),
                        CronExpressionException.NotSupported.class, "The expression is not supported."
                ),
                Arguments.of("If expression have max value less than min value, then throw exception",
                        List.of("2-1", "1", "1", "1", "1", "doSomething"),
                        CronExpressionException.class, "The maximum value is less than the minimum value."
                )
        );
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("provideCronExpressionsAndInterpretations")
    void testControllerGetData(String message, List<String> givenExpressions, List<String> expectedInterpretations) {
        //given
        //givenExpressions
        //when
        List<String> actualInterpretations = uderTest.getInterpretations(givenExpressions);
        //then
        Assertions.assertEquals(expectedInterpretations, actualInterpretations, message);
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("provideWrongCronExpressionsAndExpectedExceptions")
    void testControllerGetData_throwException(String message, List<String> givenExpressions,
                                              Class<? extends Throwable> expectedThrowClass, String expectedMessage) {
        //given
        //givenExpressions
        //when
        Throwable actualException = Assertions.assertThrows(Throwable.class, () -> uderTest.getInterpretations(givenExpressions));
        //then
        Assertions.assertEquals(expectedThrowClass, actualException.getClass(), message);
        Assertions.assertEquals(expectedMessage, actualException.getMessage(), message);
    }

}