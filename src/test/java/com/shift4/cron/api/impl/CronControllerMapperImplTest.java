package com.shift4.cron.api.impl;

import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.dto.CronExpressionsInterpretationDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TEXT;
import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TIME;
import static com.shift4.cron.domain.model.ExpressionInterpretation.builder;

class CronControllerMapperImplTest {

    private final CronControllerMapper underTest = new CronControllerMapperImpl();

    @Test
    void testMapToCommand() {
        //given
        List<String> givenArgs = List.of("*/15", "0", "1,15", "*", "1-5", "/usr/bin/find");
        //when
        CronExpressionDto actualCronCommand = underTest.mapToExpression(givenArgs);
        //
        CronExpressionDto expectedCommand = CronExpressionDto.builder()
                .expressions(List.of("*/15", "0", "1,15", "*", "1-5", "/usr/bin/find"))
                .build();
        Assertions.assertEquals(expectedCommand, actualCronCommand);
    }

    @Test
    void testMapToResponseLines() {
        //given
        CronExpressionsInterpretationDto givenInterpretation = CronExpressionsInterpretationDto.builder()
                .expressionInterpretations(List.of(
                        builder().name("minute").type(TIME).values(List.of(0, 15, 30, 45)).build(),
                        builder().name("hour").type(TIME).values(List.of(0)).build(),
                        builder().name("day of month").type(TIME).values(List.of(1, 15)).build(),
                        builder().name("month").type(TIME).values(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)).build(),
                        builder().name("day of week").type(TIME).values(List.of(1, 2, 3, 4, 5)).build(),
                        builder().name("command").type(TEXT).value("/usr/bin/find").build()
                ))
                .build();
        //when
        List<String> actualLines = underTest.mapToResponseLines(givenInterpretation);
        //then
        List<String> expected = List.of(
                "minute 0 15 30 45",
                "hour 0",
                "day of month 1 15",
                "month 1 2 3 4 5 6 7 8 9 10 11 12",
                "day of week 1 2 3 4 5",
                "command /usr/bin/find"
        );
        Assertions.assertEquals(expected, actualLines);
    }
}