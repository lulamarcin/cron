package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.model.ExpressionDefinition;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.shift4.cron.domain.generator.impl.CronGeneratorFixtures.Definition.getForMonth;
import static java.util.Collections.emptyList;
import static java.util.stream.IntStream.range;

abstract class AbstractCronGeneratorFactoryTest implements CronGeneratorFactory {

    @Override
    abstract public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData);

    @Override
    abstract public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData);

    protected static final ExpressionDefinition DEFAULT_DEFINITION = getForMonth();
    protected static final List<CronGeneratorFactory> EMPTY_FACTORIES = emptyList();

    protected static ExpressionDefinition getDefaultDefinition() {
        return DEFAULT_DEFINITION;
    }

    protected static List<CronGeneratorFactory> getEmptyFactories() {
        return EMPTY_FACTORIES;
    }

    protected void doTestAcceptAndCompile(String message, String givenExpression, Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, getDefaultDefinition(), givenExpression, expectedInterpretation);
    }

    protected void doTestAcceptAndCompile(String message, ExpressionDefinition definition, String givenExpression,
                                          Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, getEmptyFactories(), definition, givenExpression, expectedInterpretation);
    }

    protected void doTestAcceptAndCompile(String message, List<CronGeneratorFactory> factories,
                                          ExpressionDefinition definition, String givenExpression,
                                          Set<Integer> expectedInterpretation) {
        ExpressionDataDto givenExpressionData = ExpressionDataDto.builder()
                .expression(givenExpression)
                .definition(definition)
                .build();
        //when
        boolean actualAccept = accept(factories, givenExpressionData);
        Optional<CronGenerator> actualGenerator = compile(factories, givenExpressionData);
        //then
        Assertions.assertEquals(expectedInterpretation != null, actualAccept, message);
        Assertions.assertEquals(actualAccept, actualGenerator.isPresent(), message);
        Assertions.assertEquals(expectedInterpretation, actualGenerator.map(CronGenerator::generate).orElse(null));
    }

    protected void doTestCompileWhenThrowException(
            String message, String givenExpression, Class<? extends Throwable> expectedThrowClass,
            String expectedMessage) {
        doTestCompileWhenThrowException(message, getDefaultDefinition(), givenExpression,
                expectedThrowClass, expectedMessage);
    }

    protected void doTestCompileWhenThrowException(
            String message, ExpressionDefinition definition,
            String givenExpression, Class<? extends Throwable> expectedThrowClass, String expectedMessage) {
        doTestCompileWhenThrowException(message, getEmptyFactories(), definition, givenExpression,
                expectedThrowClass, expectedMessage);
    }

    protected void doTestCompileWhenThrowException(
            String message, List<CronGeneratorFactory> factories, ExpressionDefinition definition,
            String givenExpression, Class<? extends Throwable> expectedThrowClass, String expectedMessage) {
        //given
        ExpressionDataDto givenExpressionData = ExpressionDataDto.builder()
                .expression(givenExpression)
                .definition(definition)
                .build();
        //when
        Throwable actualCompileException = Assertions.assertThrows(Throwable.class, () -> compile(factories, givenExpressionData));
        //then
        Assertions.assertEquals(expectedThrowClass, actualCompileException.getClass(), message);
        Assertions.assertEquals(expectedMessage, actualCompileException.getMessage(), message);
    }


    protected static Set<Integer> getSetOfRange(int min, int max) {
        return range(min, max + 1).boxed().collect(Collectors.toSet());
    }

}
