package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.CronApplication;
import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.shift4.cron.domain.generator.impl.CronGeneratorFixtures.Definition.getForMonth;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@ActiveProfiles("test")
@SpringBootTest(classes = CronApplication.class, webEnvironment = NONE)
class StepGeneratorFactoryImplTestAbstract extends AbstractCronGeneratorFactoryTest {

    @Autowired
    private StepGeneratorFactoryImpl underTest;

    @Autowired
    private List<CronGeneratorFactory> factories;

    public static List<Arguments> provideArgumentsForTestAcceptAndCompile() {
        return List.of(
                Arguments.of("Step should start from first definition start range to end", "*/2", Set.of(1, 3, 5, 7, 9, 11)),
                Arguments.of("Can use alternatives values", "*/FEB", Set.of(1, 3, 5, 7, 9, 11)),
                Arguments.of("Can use range values", "1-5/FEB", Set.of(1, 3, 5)),
                Arguments.of("Step should start from first argument", "1/2", Set.of(1, 3, 5, 7, 9, 11))
        );
    }

    public static List<Arguments> provideArgumentsForTestAcceptAndCompileWhenThrowException() {
        return List.of(
                Arguments.of("Value out of range should throw Out of Range Exception", "13/2",
                        CronExpressionException.ValueOutOfRange.class, "Value is out of range.")
        );
    }


    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile should return correctly configured generator")
    @MethodSource("provideArgumentsForTestAcceptAndCompile")
    void testAcceptAndCompile(String message, String givenExpression, Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, factories, getForMonth(), givenExpression, expectedInterpretation);
    }

    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile method should throw exception")
    @MethodSource("provideArgumentsForTestAcceptAndCompileWhenThrowException")
    void testAcceptAndCompile_whenThrowException(
            String message, String givenExpression, Class<? extends Throwable> expectedThrowClass,
            String expectedMessage) {
        doTestCompileWhenThrowException(message, factories, getForMonth(), givenExpression, expectedThrowClass, expectedMessage);
    }

    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return underTest.accept(factories, expressionData);
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return underTest.compile(factories, expressionData);
    }
}