package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.CronApplication;
import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@ActiveProfiles("test")
@SpringBootTest(classes = CronApplication.class, webEnvironment = NONE)
class RangeAbstractCronGeneratorFactoryImplTest extends AbstractCronGeneratorFactoryTest {

    @Autowired
    private RangeGeneratorFactoryImpl underTest;

    private static List<Arguments> provideArgumentsForTestAcceptAndCompile() {
        return List.of(
                Arguments.of("Range token should generate all values in range", "1-2", getSetOfRange(1, 2)),
                Arguments.of("Other token is not accepted ", "1*2", null),
                Arguments.of("Null value is not accepted", null, null),
                Arguments.of("More complex values is not accepted", "1-2,3-4", null),
                Arguments.of("Range with any value complex values is not accepted", "*-*", null),
                Arguments.of("Range should accept alternatives values", "JAN-FEB", getSetOfRange(1, 2)),
                Arguments.of("Range should accept mixed min number and max alternatives values", "JAN-3",
                        getSetOfRange(1, 3)),
                Arguments.of("Range should accept mixed min alternative and max number values", "3-JUN",
                        getSetOfRange(3, 6))
        );
    }

    private static List<Arguments> provideArgumentsForTestAcceptAndCompileWhenThrowException() {
        return List.of(
                Arguments.of("Value out of range should throw Out of Range Exception", "0-20",
                        CronExpressionException.ValueOutOfRange.class, "Value is out of range.")
        );
    }


    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile should return correctly configured generator")
    @MethodSource("provideArgumentsForTestAcceptAndCompile")
    void testAcceptAndCompile(String message, String givenExpression, Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, givenExpression, expectedInterpretation);
    }

    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile method should throw exception")
    @MethodSource("provideArgumentsForTestAcceptAndCompileWhenThrowException")
    void testAcceptAndCompile_whenThrowException(
            String message, String givenExpression, Class<? extends Throwable> expectedThrowClass,
            String expectedMessage) {
        doTestCompileWhenThrowException(message, givenExpression, expectedThrowClass, expectedMessage);
    }

    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return underTest.accept(factories, expressionData);
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return underTest.compile(factories, expressionData);
    }
}