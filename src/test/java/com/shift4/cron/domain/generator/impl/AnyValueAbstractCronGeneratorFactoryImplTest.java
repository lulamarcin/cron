package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Optional;
import java.util.Set;

class AnyValueAbstractCronGeneratorFactoryImplTest extends AbstractCronGeneratorFactoryTest {

    private final static String ANY_VALUE_TOKEN = "*";

    private final static CronGeneratorFactory.Final UNDER_TEST = new AnyValueGeneratorFactoryImpl(ANY_VALUE_TOKEN);


    private static List<Arguments> provideArgumentsForTestAcceptAndCompile() {
        return List.of(
                Arguments.of("Any token should generate all range", ANY_VALUE_TOKEN, getSetOfRange(1, 12)),
                Arguments.of("Other token is not accepted ", "-", null),
                Arguments.of("Null value is not accepted", null, null),
                Arguments.of("More complex values is not accepted", "*,*", null),
                Arguments.of("More complex values is not accepted", "*,*", null),
                Arguments.of("Range with any value complex values is not accepted", "*-*", null)
        );
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("provideArgumentsForTestAcceptAndCompile")
    void testAcceptAndCompile(String message, String expression, Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, expression, expectedInterpretation);
    }

    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return UNDER_TEST.accept(factories, expressionData);
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return UNDER_TEST.compile(factories, expressionData);
    }
}