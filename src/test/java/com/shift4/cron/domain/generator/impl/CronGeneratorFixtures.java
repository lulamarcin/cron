package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.model.ExpressionDefinition;

import java.util.List;

import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TIME;

public class CronGeneratorFixtures {

    static class Definition {
        private static final ExpressionDefinition MONTH = ExpressionDefinition.builder()
                .name("month")
                .type(TIME)
                .range("1-12")
                .alternatives(List.of(List.of(
                        "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")))
                .build();

        static ExpressionDefinition getForMonth() {
            return MONTH;
        }

    }


}
