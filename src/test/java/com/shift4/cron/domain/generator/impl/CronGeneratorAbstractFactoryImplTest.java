package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.CronApplication;
import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.shift4.cron.domain.generator.impl.CronGeneratorFixtures.Definition.getForMonth;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@ActiveProfiles("test")
@SpringBootTest(classes = CronApplication.class, webEnvironment = NONE)
class CronGeneratorAbstractFactoryImplTest extends AbstractCronGeneratorFactoryTest {

    @Autowired
    private CronGeneratorAbstractFactoryImpl underTest;

    @Autowired
    private List<CronGeneratorFactory> factories;

    public static List<Arguments> provideArgumentsForTestAcceptAndCompile() {
        return List.of(
                Arguments.of("Two simple expression should generate sum of expressions", "1,2", Set.of(1, 2)),
                Arguments.of("Range and single expression generate sum of expressions", "1-2,3", Set.of(1, 2, 3)),
                Arguments.of("Range and step expression generate sum of expressions", "1-2,3/3", Set.of(1, 2, 3, 6, 9, 12)),
                Arguments.of("Step and step expression generate sum of expressions", "2/4,3/3", Set.of(2, 3, 6, 9, 10, 12))
        );
    }

    public static List<Arguments> provideArgumentsForTestAcceptAndCompileWhenThrowException() {
        return List.of(
                Arguments.of("Value out of range should throw Out of Range Exception", "0,13",
                        CronExpressionException.ValueOutOfRange.class, "Value is out of range."),
                Arguments.of("Not defined value throw not supported exception", "1,TEST",
                        CronExpressionException.NotSupported.class, "The expression is not supported.")
        );
    }


    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile should return correctly configured generator")
    @MethodSource("provideArgumentsForTestAcceptAndCompile")
    void testAcceptAndCompile(String message, String givenExpression, Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, factories, getForMonth(), givenExpression, expectedInterpretation);
    }

    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile method should throw exception")
    @MethodSource("provideArgumentsForTestAcceptAndCompileWhenThrowException")
    void testAcceptAndCompile_whenThrowException(
            String message, String givenExpression, Class<? extends Throwable> expectedThrowClass,
            String expectedMessage) {
        doTestCompileWhenThrowException(message, factories, getForMonth(), givenExpression, expectedThrowClass, expectedMessage);
    }


    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        try {
            underTest.compile(expressionData);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return Optional.of(underTest.compile(expressionData));
    }
}