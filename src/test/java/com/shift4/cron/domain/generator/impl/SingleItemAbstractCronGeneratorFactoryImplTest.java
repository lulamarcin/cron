package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.CronApplication;
import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@ActiveProfiles("test")
@SpringBootTest(classes = CronApplication.class, webEnvironment = NONE)
class SingleItemAbstractCronGeneratorFactoryImplTest extends AbstractCronGeneratorFactoryTest {

    @Autowired
    private SingleItemGeneratorFactoryImpl underTest;

    public static List<Arguments> provideArgumentsForTestAcceptAndCompile() {
        return List.of(
                Arguments.of("Expression of element should return this element.", "1", Set.of(1)),
                Arguments.of("Null should not generate any values.", null, null),
                Arguments.of("Support alternatives values.", "JAN", Set.of(1)),
                Arguments.of("Not support alternatives not defined values.", "JANN", null),
                Arguments.of("Not support range token.", "-", null),
                Arguments.of("Not support any value token.", "*", null)
        );
    }

    public static List<Arguments> provideArgumentsForTestAcceptAndCompileWhenThrowException() {
        return List.of(
                Arguments.of("Value out of range should throw Out of Range Exception", "13",
                        CronExpressionException.ValueOutOfRange.class, "Value is out of range.")
        );
    }

    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile should return correctly configured generator")
    @MethodSource("provideArgumentsForTestAcceptAndCompile")
    void testAcceptAndCompile(String message, String givenExpression, Set<Integer> expectedInterpretation) {
        doTestAcceptAndCompile(message, givenExpression, expectedInterpretation);
    }

    @ParameterizedTest(name = "{0}")
    @DisplayName("Compile method should throw exception")
    @MethodSource("provideArgumentsForTestAcceptAndCompileWhenThrowException")
    void testAcceptAndCompile_whenThrowException(
            String message, String givenExpression, Class<? extends Throwable> expectedThrowClass,
            String expectedMessage) {
        doTestCompileWhenThrowException(message, givenExpression, expectedThrowClass, expectedMessage);
    }

    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return underTest.accept(factories, expressionData);
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return underTest.compile(factories, expressionData);
    }
}