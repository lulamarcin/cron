package com.shift4.cron.domain.impl;

import com.shift4.cron.domain.CronService;
import com.shift4.cron.domain.config.CronConfiguration;
import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.generator.CronGeneratorAbstractFactory;
import com.shift4.cron.domain.model.ExpressionDefinition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Set;

import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TEXT;
import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TIME;
import static org.mockito.ArgumentMatchers.any;


class CronServiceImplTest {

    private final CronGeneratorAbstractFactory abstractFactory = Mockito.mock(CronGeneratorAbstractFactory.class);

    private final CronConfiguration configuration = Mockito.mock(CronConfiguration.class);

    private final CronService underTest = new CronServiceImpl(abstractFactory, configuration);

    @Test
    void test_whenInterpretCronExpressions_thenResultsAreSorted() {
        //given
        Mockito.when(abstractFactory.compile(any())).thenReturn(() -> Set.of(3, 1, 20, 2));
        Mockito.when(configuration.getExpressionDefinitions()).thenReturn(List.of(
                ExpressionDefinition.builder().type(TIME).build(),
                ExpressionDefinition.builder().type(TIME).build(),
                ExpressionDefinition.builder().type(TIME).build(),
                ExpressionDefinition.builder().type(TIME).build(),
                ExpressionDefinition.builder().type(TIME).build(),
                ExpressionDefinition.builder().type(TEXT).build()
        ));
        //when
        List<Integer> actualInterpretationValues = underTest.interpretCronExpressions(CronExpressionDto.builder()
                        .expressions(List.of("1", "1", "1", "1", "1", "1"))
                        .build())
                .getExpressionInterpretations()
                .get(0)
                .values();
        //then
        Assertions.assertEquals(List.of(1, 2, 3, 20), actualInterpretationValues);
    }


}