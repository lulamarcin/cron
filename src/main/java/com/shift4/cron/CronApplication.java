package com.shift4.cron;

import com.shift4.cron.api.CronController;
import com.shift4.cron.domain.exception.CronExpressionException;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import static java.util.Arrays.asList;

@SpringBootApplication
@RequiredArgsConstructor
public class CronApplication {

    public static void main(String[] args) {
        SpringApplication.run(CronApplication.class, args);
    }

    @Bean
    @Profile("!test")
    CommandLineRunner commandLineRunner(CronController controller) {
        return args -> {
            try {
                System.out.printf(String.join("\n", controller.getInterpretations(asList(args))));
            } catch (CronExpressionException exception) {
                System.out.printf("Field '%s' parsing error, value [%s] is invalid: received error: '%s'",
                        exception.getExpressionData().definition().name(),
                        exception.getExpressionData().expression(),
                        exception.getMessage()
                );
            }
        };
    }

}
