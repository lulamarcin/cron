package com.shift4.cron.domain.dto;

import com.shift4.cron.domain.model.ExpressionDefinition;
import lombok.Builder;

@Builder(toBuilder = true)
public record ExpressionDataDto(
        ExpressionDefinition definition,
        String expression
) {
}
