package com.shift4.cron.domain.dto;

import lombok.Builder;

import java.util.List;

@Builder
public record CronExpressionDto(
        List<String> expressions
) {
}
