package com.shift4.cron.domain.dto;

import com.shift4.cron.domain.model.ExpressionInterpretation;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CronExpressionsInterpretationDto {
    List<ExpressionInterpretation> expressionInterpretations;

}
