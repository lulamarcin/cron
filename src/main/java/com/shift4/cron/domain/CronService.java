package com.shift4.cron.domain;

import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.dto.CronExpressionsInterpretationDto;

public interface CronService {
    CronExpressionsInterpretationDto interpretCronExpressions(CronExpressionDto cronExpression);
}
