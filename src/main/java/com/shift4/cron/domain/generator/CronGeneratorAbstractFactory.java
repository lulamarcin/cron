package com.shift4.cron.domain.generator;

import com.shift4.cron.domain.dto.ExpressionDataDto;

public interface CronGeneratorAbstractFactory {
    CronGenerator compile(ExpressionDataDto expressionData);
}
