package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.model.Range;

import java.util.List;
import java.util.Optional;

interface GeneratorUtil {
    List<CronGenerator> getCronGenerators(List<CronGeneratorFactory> factories, ExpressionDataDto expressionDataDto, String[] extractedExpressions);

    Optional<Range> parseToRange(ExpressionDataDto expressionDataDto, String rangeToken);

    Optional<Integer> parseToInt(ExpressionDataDto expressionDataDto);
}
