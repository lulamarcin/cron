package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;

import java.util.List;
import java.util.Optional;

interface CronGeneratorFactory {

    boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData);

    Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData);

    interface Final extends CronGeneratorFactory {

        default boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
            return accept(expressionData);
        }

        boolean accept(ExpressionDataDto expressionData);

        default Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
            return compile(expressionData);
        }

        Optional<CronGenerator> compile(ExpressionDataDto expressionData);

    }
}
