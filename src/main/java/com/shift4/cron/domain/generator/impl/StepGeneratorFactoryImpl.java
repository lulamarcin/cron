package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.model.Range;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Component
class StepGeneratorFactoryImpl implements CronGeneratorFactory {

    public static final String NUMBER_PATTERN = "^[^\\%s]+\\%s[0-9]+$";
    private static final String ALTERNATIVES_PATTERN = "^[^\\%s]+\\%s([0-9]+|%s)+$";
    private final String stepValueToken;

    private final GeneratorUtil util;

    private final Pattern numberPattern;

    StepGeneratorFactoryImpl(String stepValueToken, GeneratorUtil util) {
        this.stepValueToken = stepValueToken;
        this.util = util;
        this.numberPattern = Pattern.compile(format(NUMBER_PATTERN, stepValueToken, stepValueToken));
    }

    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        if (!preliminaryAccept(expressionData)) {
            return false;
        }
        ExpressionDataDto extractedExpressionData = expressionData.toBuilder()
                .expression(expressionData.expression().split(stepValueToken)[0])
                .build();
        return getFilterFinalFactoriesAndReplaceExcepted(factories).stream()
                .anyMatch(factory -> factory.accept(extractedExpressionData));
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        if (!accept(factories, expressionData)) {
            return Optional.empty();
        }
        List<CronGeneratorFactory> finalFactories = getFilteredFinalFactoriesAndReplaceExcepted(factories);
        String[] rangeElements = expressionData.expression().split(stepValueToken);
        List<CronGenerator> generators = util.getCronGenerators(finalFactories, expressionData, new String[]{rangeElements[0]});
        return util.parseToInt(expressionData.toBuilder()
                        .expression(rangeElements[1])
                        .build())
                .map(step -> createGenerator(expressionData.definition().getRange(), generators, step));
    }

    private boolean preliminaryAccept(ExpressionDataDto expressionData) {
        if (numberPattern.matcher(expressionData.expression()).matches()) {
            return true;
        }
        return expressionData.definition()
                .getJoinedAlternativeValues("|")
                .map(joinedAlternativeValues -> Pattern.compile(String.format(
                        ALTERNATIVES_PATTERN,
                        stepValueToken,
                        stepValueToken,
                        joinedAlternativeValues
                )))
                .filter(pattern -> pattern.matcher(expressionData.expression()).matches())
                .isPresent();
    }

    private List<Final> getFilterFinalFactoriesAndReplaceExcepted(List<CronGeneratorFactory> factories) {
        return getFilteredFinalFactoriesAndReplaceExcepted(factories).stream()
                .map(factory -> (Final) factory)
                .toList();
    }

    private List<CronGeneratorFactory> getFilteredFinalFactoriesAndReplaceExcepted(List<CronGeneratorFactory> factories) {
        return factories.stream()
                .filter(factory -> factory instanceof Final)
                .map(factory -> factory instanceof SingleItemGeneratorFactoryImpl
                        ? new SingleItemGeneratorFactoryInStepFactoryImpl()
                        : factory
                )
                .toList();
    }

    private static CronGenerator createGenerator(Range definitionRange, List<CronGenerator> generators, int step) {
        return () -> generators.stream()
                .map(CronGenerator::generate)
                .map(Range::of)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(generatedRange -> generateStepValues(definitionRange, generatedRange, step))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    private static List<Integer> generateStepValues(Range definitionRange, Range generatedRange, int step) {
        List<Integer> result = new LinkedList<>();
        for (int i = Math.max(generatedRange.min(), definitionRange.min());
             i <= generatedRange.max() && i <= definitionRange.max(); i += step) {
            result.add(i);
        }
        return result;
    }

    //this is exception from rules - single value generator unfortunately working differently in step generator
    private class SingleItemGeneratorFactoryInStepFactoryImpl extends SingleItemGeneratorFactoryImpl {

        public SingleItemGeneratorFactoryInStepFactoryImpl() {
            super(util);
        }

        @Override
        public Optional<CronGenerator> compile(ExpressionDataDto expressionData) {
            return super.compile(expressionData)
                    .map(generator -> () -> Set.copyOf(Range.of(
                                    generator.generate().stream().findFirst().orElseThrow(),
                                    expressionData.definition().getRange().max()
                            )
                            .generateElements()));
        }
    }

}
