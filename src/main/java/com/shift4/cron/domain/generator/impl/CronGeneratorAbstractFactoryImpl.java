package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.generator.CronGeneratorAbstractFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
class CronGeneratorAbstractFactoryImpl implements CronGeneratorAbstractFactory {

    private final List<CronGeneratorFactory> factories;

    @Override
    public CronGenerator compile(ExpressionDataDto expressionData) {
        List<CronGenerator> cronGenerators = factories
                .stream()
                .map(factory -> factory.compile(factories, expressionData))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        if (cronGenerators.isEmpty()) {
            throw new CronExpressionException.NotSupported(expressionData);
        }
        return createGenerator(cronGenerators);
    }

    private static CronGenerator createGenerator(List<CronGenerator> cronGenerators) {
        return () -> cronGenerators.stream()
                .map(CronGenerator::generate)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());

    }

}
