package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.model.ExpressionDefinition;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;

@Component
@RequiredArgsConstructor
class AnyValueGeneratorFactoryImpl implements CronGeneratorFactory.Final {

    private final String anyValueToken;

    @Override
    public boolean accept(ExpressionDataDto expressionData) {
        return anyValueToken.equals(expressionData.expression());
    }

    @Override
    public Optional<CronGenerator> compile(ExpressionDataDto expressionData) {
        return Optional.of(expressionData)
                .filter(data -> accept(expressionData))
                .map(ExpressionDataDto::definition)
                .map(AnyValueGeneratorFactoryImpl::getCronGenerator);
    }

    private static CronGenerator getCronGenerator(ExpressionDefinition definition) {
        return () -> new HashSet<>(definition.getRange().generateElements());
    }
}
