package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
class SingleItemGeneratorFactoryImpl implements CronGeneratorFactory.Final {

    private static final Pattern PATTERN = Pattern.compile("^[0-9]+$");

    private static final String ALTERNATIVES_PATTERN = "^(%s)$";

    private final GeneratorUtil util;

    @Override
    public boolean accept(ExpressionDataDto expressionData) {
        if (expressionData.expression() == null) {
            return false;
        }
        if (PATTERN.matcher(expressionData.expression()).matches()) {
            return true;
        }
        for (List<String> alternative : expressionData.definition().getAlternatives()) {
            String alternativeValues = String.join("|", alternative);
            Pattern pattern = Pattern.compile(String.format(ALTERNATIVES_PATTERN, alternativeValues));
            if (pattern.matcher(expressionData.expression()).matches()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Optional<CronGenerator> compile(ExpressionDataDto expressionData) {
        if (!accept(expressionData)) {
            return Optional.empty();
        }
        return util.parseToInt(expressionData)
                .map(value -> () -> Set.of(value));
    }

}
