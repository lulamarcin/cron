package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.model.Range;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Component
class GeneratorUtilImpl implements GeneratorUtil {

    private static final Pattern NUMBER_PATTERN = Pattern.compile("^[0-9]+$");

    @Override
    public List<CronGenerator> getCronGenerators(List<CronGeneratorFactory> factories, ExpressionDataDto expressionDataDto, String[] extractedExpressions) {
        return Arrays.stream(extractedExpressions)
                .map(extractedExpression -> expressionDataDto.toBuilder()
                        .expression(extractedExpression)
                        .build())
                .map(extractedExpressionData -> {
                    List<CronGenerator> cronGeneratorFactories = factories.stream()
                            .map(factory -> factory.compile(factories, extractedExpressionData))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .toList();
                    if (cronGeneratorFactories.isEmpty()) {
                        throw new CronExpressionException.NotSupported(extractedExpressionData);
                    }
                    return cronGeneratorFactories;
                })
                .flatMap(Collection::stream)
                .toList();
    }

    @Override
    public Optional<Range> parseToRange(ExpressionDataDto expressionDataDto, String rangeToken) {
        return Optional.of(expressionDataDto.expression().split(rangeToken))
                .filter(rangeMinMaxValues -> rangeMinMaxValues.length == 2)
                .map(rangeMinMaxValues -> parseToInt(expressionDataDto.toBuilder()
                        .expression(rangeMinMaxValues[0])
                        .build())
                        .map(minValue -> parseToInt(expressionDataDto.toBuilder()
                                .expression(rangeMinMaxValues[1])
                                .build())
                                .map(maxValue -> Range.builder()
                                        .min(minValue)
                                        .max(maxValue)
                                        .build())
                        )
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(range -> {
                    if (range.min() > range.max()) {
                        throw new CronExpressionException(expressionDataDto, "The maximum value is less than the minimum value.");
                    } else {
                        return true;
                    }
                });
    }

    @Override
    public Optional<Integer> parseToInt(ExpressionDataDto expressionData) {
        if (NUMBER_PATTERN.matcher(expressionData.expression()).matches()) {
            int value = Integer.parseInt(expressionData.expression());
            if (!expressionData.definition().getRange().contain(value)) {
                throw new CronExpressionException.ValueOutOfRange(expressionData);
            }
            return Optional.of(value);
        }
        return expressionData.definition()
                .getAlternativeValue(expressionData.expression());
    }

}
