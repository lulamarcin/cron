package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGenerator;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
class ValueSeparatorGeneratorFactoryImpl implements CronGeneratorFactory {

    public static final String PATTERN = "^[^%s]+(%s[^%s]+)+$";
    private final String valueSeparatorToken;

    private final GeneratorUtil util;
    private final Pattern pattern;

    public ValueSeparatorGeneratorFactoryImpl(String valueSeparatorToken, GeneratorUtil util) {
        this.valueSeparatorToken = valueSeparatorToken;
        this.util = util;
        this.pattern = Pattern.compile(String.format(
                PATTERN,
                valueSeparatorToken,
                valueSeparatorToken,
                valueSeparatorToken
        ));
    }

    @Override
    public boolean accept(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return pattern.matcher(expressionData.expression()).matches();
    }

    @Override
    public Optional<CronGenerator> compile(List<CronGeneratorFactory> factories, ExpressionDataDto expressionData) {
        return Optional.of(expressionData.expression()
                        .split(valueSeparatorToken))
                .filter(extractedExpressions -> extractedExpressions.length > 1)
                .map(extractedExpressions -> util.getCronGenerators(factories, expressionData, extractedExpressions))
                .map(this::createGenerator);
    }

    private CronGenerator createGenerator(List<CronGenerator> cronGenerators) {
        return () -> cronGenerators.stream()
                .map(CronGenerator::generate)
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

}
