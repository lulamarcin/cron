package com.shift4.cron.domain.generator.impl;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.exception.CronExpressionException;
import com.shift4.cron.domain.generator.CronGenerator;
import com.shift4.cron.domain.model.Range;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.regex.Pattern;

import static java.lang.String.format;

@Component
class RangeGeneratorFactoryImpl implements CronGeneratorFactory.Final {

    public static final String NUMBER_PATTERN = "^[0-9]+%s[0-9]+$";
    private static final String ALTERNATIVES_PATTERN = "^([0-9]+|%s)%s([0-9]+|%s)$";

    private final GeneratorUtil util;
    private final String rangeOfValueToken;
    private final Pattern numberPattern;

    RangeGeneratorFactoryImpl(GeneratorUtil util, String rangeOfValueToken) {
        this.util = util;
        this.rangeOfValueToken = rangeOfValueToken;
        this.numberPattern = Pattern.compile(format(NUMBER_PATTERN, rangeOfValueToken));
    }

    @Override
    public boolean accept(ExpressionDataDto expressionData) {
        if (expressionData.expression() == null) {
            return false;
        }
        if (numberPattern.matcher(expressionData.expression()).matches()) {
            return true;
        }
        return expressionData.definition().getJoinedAlternativeValues("|")
                .map(joinedAlternativesValues -> Pattern.compile(format(
                        ALTERNATIVES_PATTERN,
                        joinedAlternativesValues,
                        rangeOfValueToken,
                        joinedAlternativesValues)))
                .filter(pattern -> pattern.matcher(expressionData.expression()).matches())
                .isPresent();
    }

    @Override
    public Optional<CronGenerator> compile(ExpressionDataDto expressionData) {
        if (!accept(expressionData)) {
            return Optional.empty();
        }
        return util.parseToRange(expressionData, rangeOfValueToken)
                .filter(range -> {
                    if (!expressionData.definition().getRange().containsAll(range)) {
                        throw new CronExpressionException.ValueOutOfRange(expressionData);
                    } else {
                        return true;
                    }
                })
                .map(RangeGeneratorFactoryImpl::createGenerator);
    }

    private static CronGenerator createGenerator(Range range) {
        return () -> new HashSet<>(range.generateElements());
    }
}
