package com.shift4.cron.domain.generator;

import java.util.Set;

public interface CronGenerator {
    Set<Integer> generate();

}
