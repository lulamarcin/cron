package com.shift4.cron.domain.model;

import lombok.Builder;

import java.util.List;

import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TIME;

@Builder
public record ExpressionInterpretation(
        String name,
        ExpressionDefinition.Type type,
        List<Integer> values,
        String value

) {

    public String getValueString(String delimiter) {
        return type == TIME ? String.join(
                delimiter,
                values.stream()
                        .map(Object::toString)
                        .toList())
                : value;
    }

}
