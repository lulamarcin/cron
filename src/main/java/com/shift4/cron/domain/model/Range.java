package com.shift4.cron.domain.model;

import lombok.Builder;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import static java.lang.Integer.parseInt;

@Builder
public record Range(
        int min,
        int max
) {
    public static Optional<Range> of(String value, String token) {
        String[] minMax = value.split(token);
        if (minMax.length != 2) {
            return Optional.empty();
        }
        return Optional.of(of(parseInt(minMax[0]), parseInt(minMax[1])));
    }

    public static Range of(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("Invalid range. Min value is greater than max value.");
        }
        return Range.builder()
                .min(min)
                .max(max)
                .build();
    }


    public static Optional<Range> of(Collection<Integer> values) {
        return values.stream()
                .min(Integer::compare)
                .flatMap(min -> values.stream().max(Integer::compare)
                        .map(max -> Range.of(min, max))
                );
    }

    public List<Integer> generateElements() {
        return IntStream.range(min, max + 1)
                .boxed()
                .toList();
    }

    public boolean containsAll(Range range) {
        return min <= range.min && range.max() <= max;
    }

    public boolean contain(int value) {
        return min <= value && value <= max;
    }

}
