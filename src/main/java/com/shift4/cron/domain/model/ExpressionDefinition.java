package com.shift4.cron.domain.model;

import lombok.Builder;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.function.Predicate.not;

@Builder
public record ExpressionDefinition(
        String name,
        Type type,
        String range,
        List<List<String>> alternatives
) {

    private static final String RANGE_TOKEN = "-";

    public enum Type {
        TIME,
        TEXT
    }

    public List<List<String>> getAlternatives() {
        return Optional.ofNullable(alternatives)
                .orElse(emptyList());
    }

    public Optional<Integer> getAlternativeValue(String expression) {
        Range range = getRange();
        for (List<String> alternativeValues : getAlternatives()) {
            int index = range.min();
            for (String alternativeValue : alternativeValues) {
                if (alternativeValue.equals(expression)) {
                    return Optional.of(index);
                }
                index++;
            }
        }
        return Optional.empty();

    }

    public Optional<String> getJoinedAlternativeValues(String delimiter) {
        String joinedAlternativeValues = String.join(delimiter, getAlternatives().stream()
                .map(alternatives -> String.join(delimiter, alternatives))
                .collect(Collectors.toSet()));
        return Optional.of(joinedAlternativeValues)
                .filter(not(String::isEmpty));
    }

    public Range getRange() {
        return Range.of(range(), RANGE_TOKEN).orElseThrow();
    }

}
