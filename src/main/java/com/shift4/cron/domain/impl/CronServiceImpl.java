package com.shift4.cron.domain.impl;

import com.shift4.cron.domain.CronService;
import com.shift4.cron.domain.config.CronConfiguration;
import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.dto.CronExpressionsInterpretationDto;
import com.shift4.cron.domain.dto.ExpressionDataDto;
import com.shift4.cron.domain.generator.CronGeneratorAbstractFactory;
import com.shift4.cron.domain.model.ExpressionDefinition;
import com.shift4.cron.domain.model.ExpressionInterpretation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TEXT;
import static com.shift4.cron.domain.model.ExpressionDefinition.Type.TIME;

@Service
@RequiredArgsConstructor
class CronServiceImpl implements CronService {

    private final CronGeneratorAbstractFactory factory;

    private final CronConfiguration configuration;

    @Override
    public CronExpressionsInterpretationDto interpretCronExpressions(CronExpressionDto cronExpression) {
        if (configuration.getExpressionDefinitions().size() != cronExpression.expressions().size()) {
            throw new IllegalArgumentException("Incompatible number of arguments.");
        }
        List<ExpressionInterpretation> expressionInterpretations = new ArrayList<>(configuration.getExpressionDefinitions().size());
        Iterator<ExpressionDefinition> definitionsIterator = configuration.getExpressionDefinitions().iterator();
        Iterator<String> expressionsIterator = cronExpression.expressions().iterator();
        while (definitionsIterator.hasNext() && expressionsIterator.hasNext()) {
            ExpressionDefinition expressionDefinition = definitionsIterator.next();
            String expression = expressionsIterator.next();
            ExpressionInterpretation interpretation = generateInterpretation(expressionDefinition, expression);
            expressionInterpretations.add(interpretation);
        }
        return CronExpressionsInterpretationDto.builder()
                .expressionInterpretations(expressionInterpretations)
                .build();
    }

    private ExpressionInterpretation generateInterpretation(ExpressionDefinition definition, String expression) {
        ExpressionInterpretation.ExpressionInterpretationBuilder interpretationBuilder = ExpressionInterpretation
                .builder()
                .name(definition.name())
                .type(definition.type());
        if (definition.type() == TIME) {
            List<Integer> values = factory.compile(ExpressionDataDto.builder()
                            .definition(definition)
                            .expression(expression)
                            .build())
                    .generate()
                    .stream()
                    .sorted()
                    .toList();
            interpretationBuilder.values(values);
        } else if (definition.type() == TEXT) {
            interpretationBuilder.value(expression);
        }
        return interpretationBuilder.build();
    }

}
