package com.shift4.cron.domain.config;

import com.shift4.cron.domain.model.ExpressionDefinition;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

import static lombok.AccessLevel.PACKAGE;

@Getter
@Setter(PACKAGE)
@Configuration
@ConfigurationProperties(prefix = "cron")
@NoArgsConstructor(access = PACKAGE)
public class CronConfiguration {

    private Token token;

    private List<ExpressionDefinition> expressionDefinitions;

    public record Token(
            String valueSeparator,
            String anyValue,
            String rangeOfValue,
            String stepValue
    ) {
    }

    @Bean
    String valueSeparatorToken(CronConfiguration configuration) {
        return configuration.getToken().valueSeparator;
    }

    @Bean
    String anyValueToken(CronConfiguration configuration) {
        return configuration.getToken().anyValue;
    }

    @Bean
    String rangeOfValueToken(CronConfiguration configuration) {
        return configuration.getToken().rangeOfValue;
    }

    @Bean
    String stepValueToken(CronConfiguration configuration) {
        return configuration.getToken().stepValue;
    }

}
