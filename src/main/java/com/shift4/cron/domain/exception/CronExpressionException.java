package com.shift4.cron.domain.exception;

import com.shift4.cron.domain.dto.ExpressionDataDto;
import lombok.Getter;

@Getter
public class CronExpressionException extends RuntimeException {

    private final ExpressionDataDto expressionData;

    public CronExpressionException(ExpressionDataDto expressionData, String message) {
        super(message);
        this.expressionData = expressionData;
    }

    public static class NotSupported extends CronExpressionException {

        public NotSupported(ExpressionDataDto expressionData) {
            super(expressionData, "The expression is not supported.");
        }
    }

    public static class ValueOutOfRange extends CronExpressionException {

        public ValueOutOfRange(ExpressionDataDto expressionData) {
            super(expressionData, "Value is out of range.");
        }
    }

}
