package com.shift4.cron.api.impl;

import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.dto.CronExpressionsInterpretationDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
class CronControllerMapperImpl implements CronControllerMapper {
    @Override
    public CronExpressionDto mapToExpression(List<String> expressions) {
        return CronExpressionDto.builder()
                .expressions(expressions)
                .build();
    }

    @Override
    public List<String> mapToResponseLines(CronExpressionsInterpretationDto interpretation) {
        return interpretation.getExpressionInterpretations()
                .stream()
                .map(expressionInterpretation -> String.format("%s %s",
                        expressionInterpretation.name(),
                        expressionInterpretation.getValueString(" "))
                )
                .toList();
    }

}
