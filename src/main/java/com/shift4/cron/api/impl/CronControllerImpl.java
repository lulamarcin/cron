package com.shift4.cron.api.impl;

import com.shift4.cron.api.CronController;
import com.shift4.cron.domain.CronService;
import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.dto.CronExpressionsInterpretationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
@RequiredArgsConstructor
class CronControllerImpl implements CronController {

    private final CronService cronService;
    private final CronControllerMapper mapper;

    @Override
    public List<String> getInterpretations(List<String> expressions) {
        CronExpressionDto expression = mapper.mapToExpression(expressions);
        CronExpressionsInterpretationDto interpretation = cronService.interpretCronExpressions(expression);
        return mapper.mapToResponseLines(interpretation);
    }

}
