package com.shift4.cron.api.impl;

import com.shift4.cron.domain.dto.CronExpressionDto;
import com.shift4.cron.domain.dto.CronExpressionsInterpretationDto;

import java.util.List;

interface CronControllerMapper {
    CronExpressionDto mapToExpression(List<String> expressions);

    List<String> mapToResponseLines(CronExpressionsInterpretationDto interpretation);
}
