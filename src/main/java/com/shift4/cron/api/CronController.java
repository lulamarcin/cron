package com.shift4.cron.api;

import java.util.List;

public interface CronController {
    List<String> getInterpretations(List<String> expressions);
}
